
db.users.find(
        {
            $or: [
                {firstName: {$regex: 's'}},
                {firstName: {$regex: 'S'}},
                {lastName: {$regex: 'd'}},
                {lastName: {$regex: 'D'}}
            ]
        }
)

///////////////////////////////////////////////////


db.users.find(
        {
            $and: [
                {firstName: {$regex: 'e'}},
                {age: {$lte: 30}}
            ]
        }
)

///////////////////////////////////////////////////


db.users.find(
        {
            $and: [
                {department: "HR"},
                {age: {$gte: 70}}
            ]
        }
)

